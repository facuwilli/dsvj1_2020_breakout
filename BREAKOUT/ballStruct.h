#ifndef BALLSTRUCT_H

#define BALLSTRUCT_H

#include "..\lib\raylib\include\raylib.h"

struct Ball
{
	Vector2 position;
	Vector2 speed;
	float radius;
	Color color;
	bool status;
};

#endif