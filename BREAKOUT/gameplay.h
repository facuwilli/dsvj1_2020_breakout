#ifndef GAMEPLAY_H

#define GAMEPLAY_H

#include "structPad.h"
#include "ballStruct.h"
#include "structBlock.h"
#include "ascii_values.h"
#include "game.h"

namespace ARKANOID
{
	namespace GAMEPLAY
	{
		extern Pad pad;
		extern int moveRight;
		extern int moveLeft;
		extern Ball ball;
		extern int throwBall;
		extern Block blocks[blocksAmount];

		namespace PAD
		{
			void InitPad();
			void DrawPad();
			void DrawLifes();
			void PadMovement();
			bool LimitsReached(bool side);
			void MovementStatus();
			void InitLifes();
			void AllLifesLost();

			namespace SCORE
			{
				extern short _desPoints;

				void InitScore();
				void DrawScore();
				void ScoreUp(short index);
			}
		}
		namespace BALL
		{
			extern short blockCollisionIndex;
			extern short blocksDestroy;
			extern bool onCollision;

			void InitBall();
			void BallMovement();
			void DrawBall();
			void ThrowBall();
			void CoreMechanics();
			bool CheckCollisionBlocks();
			void SpeedUp();
			void RandomDirection();
		}
		namespace BLOCKS
		{
			void InitBlocks(short normals, short resistants, short hards, short veryHards);
			void DrawBlocks();
			void DestroyBlock(short index);
			bool AllBlocksDestroy();
			void LowerBlockDurability(short blockIndex);
			void WiningAchived();
		}

		void RestartObjetsPositions();
		void RestartMatch();
		void HideCursorIfMatchStarted();
		void TurnOffMusic();
	}
}

#endif