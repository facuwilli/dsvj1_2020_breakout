#ifndef STRUCTBLOCK_H

#define STRUCTBLOCK_H

#include "..\lib\raylib\include\raylib.h"

const short blocksAmount = 72;
enum BlockType{ normal, resistant, hard, reallyHard};
const short pointsLenght = 3;
extern short normalBlocks;
extern short resistantBlocks;
extern short hardBlocks;
extern short reallyHardBlocks;

struct Block
{
	Rectangle rec;
	short destructionPoints[pointsLenght];	
	short durability;
	Color color;
	BlockType type;
	bool status;
};

#endif