#ifndef OUTPUT_VARS_H

#define OUTPUT_VARS_H

#include "raylib.h"

const int screenWidth = 960;
const int screenHeight = 960;

#pragma region Main Menu Vars
const int backgroundXPos = 0;
const int backgroundYPos = 0;
const Color backgroundColor = WHITE;

const int playRecWidth = 275;
const int playRecHeight = 100;
const int playRecX_Pos = screenWidth / 2 - (playRecWidth / 2);
const int playRecY_Pos = (screenHeight / 2) - 300;
const Color playRecColor = RED;

const int creditsRecWidth = 275;
const int creditsRecHeight = 100;
const int creditsRecX_Pos = screenWidth / 2 - (playRecWidth / 2);
const int creditsRecY_Pos = (screenHeight / 2) - 150;
const Color creditsRecColor = RED;

const int instructionsRecWidth = 275;
const int instructionsRecHeight = 100;
const int instructionsRecX_Pos = screenWidth / 2 - (playRecWidth / 2);
const int instructionsRecY_Pos = (screenHeight / 2);
const Color instructionsRecColor = RED;

const int exitRecWidth = 275;
const int exitRecHeight = 100;
const int exitRecX_Pos = screenWidth / 2 - (playRecWidth / 2);
const int exitRecY_Pos = (screenHeight / 2) + 150;
const Color exitRecColor = RED;

const char playText[6] = { "START" };
const int playTextFontSize = 50;
const int playTextX_Pos = playRecX_Pos + (playRecWidth / 6);
const int playTextY_Pos = playRecY_Pos + (playRecHeight / 4);
const Color playTextColor = DARKBLUE;

const char creditsText[8] = { "CREDITS" };
const int creditsTextFontSize = 50;
const int creditsTextX_Pos = playRecX_Pos + (playRecWidth / 6) - 20;
const int creditsTextY_Pos = playRecY_Pos + (playRecHeight / 4) + 150;
const Color creditsTextColor = DARKBLUE;

const char instructionsText[13] = { "INSTRUCTIONS" };
const int instructionsTextFontSize = 34;
const int instructionsTextX_Pos = playRecX_Pos + (playRecWidth / 6) - 40;
const int instructionsTextY_Pos = playRecY_Pos + (playRecHeight / 4) + 300;
const Color instructionsTextColor = DARKBLUE;

const char exitText[5] = { "EXIT" };
const int exitTextFontSize = 50;
const int exitTextX_Pos = playRecX_Pos + (playRecWidth / 6) + 25;
const int exitTextY_Pos = playRecY_Pos + (playRecHeight / 4) + 450;
const Color exitTextColor = DARKBLUE;

const char versionText[10] = { "GAME v1.0" };
const int versionTextFontSize = 20;
const int versionTextX_Pos = 850;
const int versionTextY_Pos = 920;
const Color versionTextColor = DARKBLUE;

const char arkanoidText[9] = { "ARKANOID" };
const int arkanoidTextFontSize = 85;
const int arkanoidTextX_Pos = playRecX_Pos + (playRecWidth / 6) - 120;
const int arkanoidTextY_Pos = 60;
const Color arkanoidTextColor = BLACK;
#pragma endregion

#pragma region Win Scene Vars
const char winText[8] = { "YOU WIN" };
const int winTextFontSize = 100;
const int winTextX_Pos = screenWidth / 2 - (screenWidth / 4);
const int winTextY_Pos = screenHeight / 2;
const Color winTextColor = DARKBLUE;

const int goBackFontSize = 30;
const int goBackTextX_Pos = screenWidth / 2 - (screenWidth / 4);
const int goBackTextY_Pos = 600;
const Color goBackTextColor = YELLOW;
const char goBackText[23] = { "PRESS ENTER TO GO BACK" };
#pragma endregion

#pragma region Loose Scene Vars
const char looseText[9] = { "YOU LOST" };
const int looseTextFontSize = 100;
const int looseTextX_Pos = screenWidth / 2 - (screenWidth / 4);
const int  looseTextY_Pos = screenHeight / 2;
const Color looseTextColor = DARKBLUE;
#pragma endregion

#pragma region Credits Scene Vars
const int creditsSceneTextFontSize = 30;
const int creditsSceneTextX_Pos = 50;
const Color creditsSceneTextColor = BLACK;

const char one_creditsText[8] = "CREDITS" ;
const int one_creditsTextY_Pos = 80;
const char two_creditsText[37] = "GAME PROGRAMED BY   FACUNDO WILLIAMS";
const int two_creditsTextY_Pos = 160;
const char three_creditsText[38] = "MAIN MENU MUSIC BY   SANTIAGO SEGARRA";
const int three_creditsTextY_Pos = 240;
const char four_creditsText[33] = "GAME MUSIC BY   WWW.BENSOUND.COM";
const int four_creditsTextY_Pos = 320;
const char five_creditsText[26] = "GAME TEXTURES BY   ZEALEX";
const int five_creditsTextY_Pos = 400;
const char six_creditsText[39] = "BACKGROUND ART BY LUIS ZUNO (@ansimuz)";
const int six_creditsTextY_Pos = 480;

const int seven_creditsTextX_Pos = 150;
const int seven_creditsTextY_Pos = 560;
const Color seven_creditsTextColor = YELLOW;
const char seven_creditsText[23] = "PRESS ENTER TO GO BACK";
#pragma endregion

#pragma region Map Limits Vars
const int limitsYPos = 0;

const int mapXPos = 120;
const int mapWidth = 720;
const int mapHeight = 960;
const Color mapColor = WHITE;

const int leftEdgeXPos = 120;
const int leftEdgeWidth = 10;
const int leftEdgeHeight = 960;
const Color leftEdgeColor = DARKGREEN;

const int rightEdgeXPos = 840;
const int rightEdgeWidth = 10;
const int rightEdgeHeight = 960;
const Color rightEdgeColor = DARKGREEN;
#pragma endregion

#pragma region Lifes Vars
const char lifesText[6] = "LIFES";
const int lifesTextXPos = 10;
const int lifesTextYPos = 860;
const int lifesTextFontSize = 30;
const Color lifesTextColor = WHITE;

const int lifesXPos = 40;
const int lifesYPos = 900;
const int lifesFontSize = 40;
const Color lifesColor = WHITE;
#pragma endregion

#pragma region Pause Vars
const int pauseBackgroundRecWidth = 325;
const int pauseBackgroundRecHeight = 500;
const int pauseBackgroundRecX_Pos = screenWidth / 2 - (pauseBackgroundRecWidth / 2);
const int pauseBackgroundRecY_Pos = (screenHeight / 2) - 350;
const Color pauseBackgroundRecColor = YELLOW;

const int resumeRecWidth = 275;
const int resumeRecHeight = 100;
const int resumeRecX_Pos = screenWidth / 2 - (resumeRecWidth / 2);
const int resumeRecY_Pos = (screenHeight / 2) - 300;
const Color resumeRecColor = RED;

const int backRecWidth = 275;
const int backRecHeight = 100;
const int backRecX_Pos = screenWidth / 2 - (backRecWidth / 2);
const int backRecY_Pos = (screenHeight / 2) - 150;
const Color backRecColor = RED;

const int restartRecWidth = 275;
const int restartRecHeight = 100;
const int restartRecX_Pos = screenWidth / 2 - (restartRecWidth / 2);
const int restartRecY_Pos = (screenHeight / 2);
const Color restartRecColor = RED;

const char resumeText[7] = "RESUME";
const int resumeTextFontSize = 50;
const int resumeTextX_Pos = resumeRecX_Pos + (resumeRecWidth / 6) - 10;
const int resumeTextY_Pos = resumeRecY_Pos + (resumeRecHeight / 4);
const Color resumeTextColor = DARKBLUE;

const char backText[8] = "GO BACK";
const int backTextFontSize = 50;
const int backTextX_Pos = backRecX_Pos + (backRecWidth / 6)- 15;
const int backTextY_Pos = backRecY_Pos + (backRecHeight / 4);
const Color backTextColor = DARKBLUE;

const char restartText[8] = "RESTART";
const int restartTextFontSize = 50;
const int restartTextX_Pos = restartRecX_Pos + (restartRecWidth / 6) - 30;
const int restartTextY_Pos = restartRecY_Pos + (restartRecHeight / 4);
const Color restartTextColor = DARKBLUE;

const char pauseText[9] = "PAUSE[P]";
const int pauseTextXPos = 860;
const int pauseTextYPos = 860;
const int pauseTextFontSize = 20;
const Color pauseTextColor = WHITE;
#pragma endregion

#pragma region Score Vars
const char scoreText[6] = "SCORE";
const int scoreTextXPos = 200;
const int scoreTextYPos = 35;
const int scoreTextFontSize = 80;
const Color scoreTextColor = WHITE;

const int scoreXPos = 520;
const int scoreYPos = 35;
const int scoreFontSize = 80;
const Color scoreColor = WHITE;
#pragma endregion

#pragma region Instructions Vars
const int instructionsSceneTextFontSize = 22;
const int instructionsSceneTextX_Pos = 30;
const Color instructionsSceneTextColor = BLACK;

const char mainInstructionsText[43] = "YOUR OBJETIVE IS TO DESTROY ALL THE BLOCKS";
const int mainInstructionsTextY_Pos = 80;
const char mainInstructionsTwoText[36] = "MAKING THE BALL BOUNCE WITH THE PAD";
const int mainInstructionsTwoTextY_Pos = 120;
const char moveInstructionsText[53] = "MOVE THE PAD WITH THE KEYPAD KEYS [RIGHT] and [LEFT]";
const int moveInstructionsTextY_Pos = 200;
const char throwBallInstructionsText[48] = "TO THROW THE BALL AT THE BEGINNIG PRESS [SPACE]";
const int throwBallInstructionsTextY_Pos = 280;
const char pauseInstructionsText[28] = "TO PAUSE THE GAME PRESS [P]";
const int pauseInstructionsTextY_Pos = 360;
const char exitInstructionsText[70] = "TO EXIT GAME YOU CAN CLICK THE EXIT OPTION ON THE MENU OR PRESS [ESC]";
const int exitInstructionsTextY_Pos = 440;
const char turnOffMusicText[36] = "TO STOP/RESUME GAME MUSIC PRESS [M]";
const int turnOffMusicTextY_Pos = 520;

const int instructionsBackTextX_Pos = 150;
const int instructionsBackTextY_Pos = 600;
const Color instructionsBackTextColor = YELLOW;
const char instructionsBackText[23] = "PRESS ENTER TO GO BACK";
#pragma endregion

#pragma region Music Vars
const char musicText[9] = "MUSIC[M]";
const int musicTextXPos = 860;
const int musicTextYPos = 890;
const int musicTextFontSize = 20;
const Color musicTextColor = WHITE;
#pragma endregion

#endif
