#include "game.h"

using namespace ARKANOID;

#pragma region vars
GAME::Scenes GAME::currentScene = GAME::Scenes::startMenu;
short normalBlocks = 18;
short resistantBlocks = 18;
short hardBlocks = 18;
short reallyHardBlocks = 18;
Music GAME::introMusic = LoadMusicStream("../res/music/intro_music.mp3");
Music GAME::gameMusic = LoadMusicStream("../res/music/bensound-endlessmotion.mp3");
Texture2D GAME::txtr_Pad;
Texture2D GAME::txtr_Ball;
Texture2D GAME::txtr_Block1;
Texture2D GAME::txtr_Block2;
Texture2D GAME::txtr_Block3;
Texture2D GAME::txtr_Block4;
Texture2D GAME::background;
bool GAME::exit = false;
#pragma endregion

void GAME::MENU::MainMenuScene()
{
	BeginDrawing();

	ClearBackground(BLACK);

	DrawTexture(background, backgroundXPos, backgroundYPos, backgroundColor);
	DrawRectangle(playRecX_Pos, playRecY_Pos, playRecWidth, playRecHeight, playRecColor);
	DrawText(playText, playTextX_Pos, playTextY_Pos, playTextFontSize, playTextColor);
	DrawRectangle(creditsRecX_Pos, creditsRecY_Pos, creditsRecWidth, creditsRecHeight, creditsRecColor);
	DrawText(creditsText, creditsTextX_Pos, creditsTextY_Pos, creditsTextFontSize, creditsTextColor);
	DrawRectangle(instructionsRecX_Pos, instructionsRecY_Pos, instructionsRecWidth, instructionsRecHeight, instructionsRecColor);
	DrawText(instructionsText, instructionsTextX_Pos, instructionsTextY_Pos, instructionsTextFontSize, instructionsTextColor);
	DrawRectangle(exitRecX_Pos, exitRecY_Pos, exitRecWidth, exitRecHeight, exitRecColor);
	DrawText(exitText, exitTextX_Pos, exitTextY_Pos, exitTextFontSize, exitTextColor);	
	DrawText(versionText, versionTextX_Pos, versionTextY_Pos, versionTextFontSize, versionTextColor);
	DrawText(arkanoidText, arkanoidTextX_Pos, arkanoidTextY_Pos, arkanoidTextFontSize, arkanoidTextColor);

	EndDrawing();
}
void GAME::MENU::WinScene()
{
	BeginDrawing();

	ClearBackground(BLACK);
	
	DrawTexture(background, backgroundXPos, backgroundYPos, backgroundColor);
	DrawText(winText, winTextX_Pos, winTextY_Pos, winTextFontSize, winTextColor);
	DrawText(goBackText, goBackTextX_Pos, goBackTextY_Pos, goBackFontSize, goBackTextColor);

	EndDrawing();
}
void GAME::MENU::LooseScene()
{
	BeginDrawing();

	ClearBackground(BLACK);
	
	DrawTexture(background, backgroundXPos, backgroundYPos, backgroundColor);
	DrawText(looseText, looseTextX_Pos, looseTextY_Pos, looseTextFontSize, looseTextColor);
	DrawText(goBackText, goBackTextX_Pos, goBackTextY_Pos, goBackFontSize, goBackTextColor);

	EndDrawing();
}
void GAME::MENU::CreditsScene()
{
	BeginDrawing();

	ClearBackground(BLACK);

	DrawTexture(background, backgroundXPos, backgroundYPos, backgroundColor);
	DrawText(one_creditsText, creditsSceneTextX_Pos, one_creditsTextY_Pos, creditsSceneTextFontSize, creditsSceneTextColor);
	DrawText(two_creditsText, creditsSceneTextX_Pos, two_creditsTextY_Pos, creditsSceneTextFontSize, creditsSceneTextColor);
	DrawText(three_creditsText, creditsSceneTextX_Pos, three_creditsTextY_Pos, creditsSceneTextFontSize, creditsSceneTextColor);
	DrawText(four_creditsText, creditsSceneTextX_Pos, four_creditsTextY_Pos, creditsSceneTextFontSize, creditsSceneTextColor);
	DrawText(five_creditsText, creditsSceneTextX_Pos, five_creditsTextY_Pos, creditsSceneTextFontSize, creditsSceneTextColor);
	DrawText(six_creditsText, creditsSceneTextX_Pos, six_creditsTextY_Pos, creditsSceneTextFontSize, creditsSceneTextColor);
	DrawText(seven_creditsText, seven_creditsTextX_Pos, seven_creditsTextY_Pos, creditsSceneTextFontSize, seven_creditsTextColor);
	
	EndDrawing();
}
void GAME::MENU::PauseMenu()
{
	BeginDrawing();

	DrawTexture(background, backgroundXPos, backgroundYPos, backgroundColor);	
	DrawRectangle(leftEdgeXPos, limitsYPos, leftEdgeWidth, leftEdgeHeight, leftEdgeColor);
	DrawRectangle(rightEdgeXPos, limitsYPos, rightEdgeWidth, rightEdgeHeight, rightEdgeColor);
	GAMEPLAY::PAD::DrawPad();
	GAMEPLAY::BALL::DrawBall();
	GAMEPLAY::BLOCKS::DrawBlocks();
	GAMEPLAY::PAD::DrawLifes();
	GAMEPLAY::PAD::SCORE::DrawScore();

	DrawRectangle(pauseBackgroundRecX_Pos, pauseBackgroundRecY_Pos, pauseBackgroundRecWidth, pauseBackgroundRecHeight, pauseBackgroundRecColor);
	DrawRectangle(resumeRecX_Pos, resumeRecY_Pos, resumeRecWidth, resumeRecHeight, resumeRecColor);
	DrawText(resumeText, resumeTextX_Pos, resumeTextY_Pos, resumeTextFontSize, resumeTextColor);
	DrawRectangle(backRecX_Pos, backRecY_Pos, backRecWidth, backRecHeight, backRecColor);
	DrawText(backText, backTextX_Pos, backTextY_Pos, backTextFontSize, backTextColor);
	DrawRectangle(restartRecX_Pos, restartRecY_Pos, restartRecWidth, restartRecHeight, restartRecColor);
	DrawText(restartText, restartTextX_Pos, restartTextY_Pos, restartTextFontSize, restartTextColor);
	
	EndDrawing();
}
void GAME::MENU::InstructionsScene()
{
	BeginDrawing();

	ClearBackground(BLACK);

	DrawTexture(background, backgroundXPos, backgroundYPos, backgroundColor);
	DrawText(mainInstructionsText, instructionsSceneTextX_Pos, mainInstructionsTextY_Pos, instructionsSceneTextFontSize, instructionsSceneTextColor);
	DrawText(mainInstructionsTwoText, instructionsSceneTextX_Pos, mainInstructionsTwoTextY_Pos, instructionsSceneTextFontSize, instructionsSceneTextColor);
	DrawText(moveInstructionsText, instructionsSceneTextX_Pos, moveInstructionsTextY_Pos, instructionsSceneTextFontSize, instructionsSceneTextColor);
	DrawText(throwBallInstructionsText, instructionsSceneTextX_Pos, throwBallInstructionsTextY_Pos, instructionsSceneTextFontSize, instructionsSceneTextColor);DrawText(moveInstructionsText, instructionsSceneTextX_Pos, moveInstructionsTextY_Pos, instructionsSceneTextFontSize, instructionsSceneTextColor);
	DrawText(pauseInstructionsText, instructionsSceneTextX_Pos, pauseInstructionsTextY_Pos, instructionsSceneTextFontSize, instructionsSceneTextColor);
	DrawText(exitInstructionsText, instructionsSceneTextX_Pos, exitInstructionsTextY_Pos, instructionsSceneTextFontSize, instructionsSceneTextColor);
	DrawText(turnOffMusicText, instructionsSceneTextX_Pos, turnOffMusicTextY_Pos, instructionsSceneTextFontSize, instructionsSceneTextColor);
	DrawText(instructionsBackText, instructionsBackTextX_Pos, instructionsBackTextY_Pos, instructionsSceneTextFontSize, instructionsBackTextColor);

	EndDrawing();
}
void GAME::MENU::KeyboardInput(int key, Scenes scene)
{
	if (IsKeyPressed(key)) 
	{
		currentScene = scene;
	}
}
void GAME::MENU::KeyboardInput(int key, Scenes scene, Music playMusic, Music stopMusic)
{
	if (IsKeyPressed(key))
	{
		currentScene = scene;
		PlayMusicStream(playMusic);
		StopMusicStream(stopMusic);
	}
}
void GAME::MENU::MouseInput(Scenes scene, int min_X, int min_Y, int max_X, int max_Y)
{
	if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON) && GetMouseX() >= min_X && GetMouseX() < max_X && GetMouseY() >= min_Y && GetMouseY() < max_Y)
	{
		currentScene = scene;		
	}
}
void GAME::MENU::MouseInput(Scenes scene, int min_X, int min_Y, int max_X, int max_Y, Music playMusic, Music stopMusic)
{	
	if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON) && GetMouseX() >= min_X && GetMouseX() < max_X && GetMouseY() >= min_Y && GetMouseY() < max_Y)
	{
		currentScene = scene;
		PlayMusicStream(playMusic);
		StopMusicStream(stopMusic);
	}	
}
void GAME::MENU::RestartMatchWithMouseInput(Scenes scene,int min_X, int min_Y, int max_X, int max_Y)
{
	if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON) && GetMouseX() >= min_X && GetMouseX() < max_X && GetMouseY() >= min_Y && GetMouseY() < max_Y)
	{
		GAMEPLAY::RestartMatch();
		currentScene = scene;
	}
}
void GAME::MENU::RestartMatchWithMouseInput(Scenes scene, int min_X, int min_Y, int max_X, int max_Y, Music playMusic, Music stopMusic)
{
	if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON) && GetMouseX() >= min_X && GetMouseX() < max_X && GetMouseY() >= min_Y && GetMouseY() < max_Y)
	{
		GAMEPLAY::RestartMatch();
		currentScene = scene;
		PlayMusicStream(playMusic);
		StopMusicStream(stopMusic);
	}
}
void GAME::MENU::ChangeCurrentScene(short scene)
{
	switch (scene)
	{
	case 0:
		currentScene = Scenes::startMenu;
		break;
	case 1:
		currentScene = Scenes::game;
		break;	
	case 2:
		currentScene = Scenes::win;		
	case 3:
		currentScene = Scenes::loose;
		break;
	case 4:
		currentScene = Scenes::credits;
		break;
	}
}
void GAME::MENU::ShowCursorOnMenu()
{
	if (IsCursorHidden())
	{
		ShowCursor();
	}
}

void GAME::Init()
{	
	InitWindow(screenWidth, screenHeight, "BREAKOUT v1.0");
	InitAudioDevice();
	txtr_Pad = LoadTexture("../res/textures/Pad.png");
	txtr_Ball = LoadTexture("../res/textures/Ball.png");
	txtr_Block1 = LoadTexture("../res/textures/Lightblue_Block.png");
	txtr_Block2 = LoadTexture("../res/textures/Yellow_Block.png");
	txtr_Block3 = LoadTexture("../res/textures/Orange_Block.png");
	txtr_Block4 = LoadTexture("../res/textures/Red_Block.png");
	background = LoadTexture("../res/images/parallax_mountain.png");

	GAMEPLAY::PAD::InitPad();
	GAMEPLAY::BALL::InitBall();
	GAMEPLAY::BLOCKS::InitBlocks(normalBlocks, resistantBlocks, hardBlocks, reallyHardBlocks);
	GAMEPLAY::PAD::SCORE::InitScore();
	
	SetMusicVolume(introMusic, 0.7f);
	SetMusicVolume(gameMusic, 0.06f);
	PlayMusicStream(introMusic);

	SetTargetFPS(60);	
}
void GAME::Input()
{
	GAME::MENU::KeyboardInput(KEY_P, Scenes::pause);
	GAMEPLAY::PAD::MovementStatus();	
	GAMEPLAY::BALL::ThrowBall();
}
void GAME::Update()
{
	GAMEPLAY::HideCursorIfMatchStarted();
	UpdateMusicStream(gameMusic);
	GAMEPLAY::PAD::PadMovement();
	GAMEPLAY::BALL::BallMovement();
	GAMEPLAY::TurnOffMusic();
}
void GAME::Draw()
{	
	BeginDrawing();

	ClearBackground(BLACK);

	DrawTexture(background, backgroundXPos, backgroundYPos, backgroundColor);	
	DrawRectangle(leftEdgeXPos, limitsYPos, leftEdgeWidth, leftEdgeHeight, leftEdgeColor);
	DrawRectangle(rightEdgeXPos, limitsYPos, rightEdgeWidth, rightEdgeHeight, rightEdgeColor);
	DrawText(pauseText, pauseTextXPos, pauseTextYPos, pauseTextFontSize, pauseTextColor);
	DrawText(musicText, musicTextXPos, musicTextYPos, musicTextFontSize, musicTextColor);
	GAMEPLAY::PAD::DrawPad();
	GAMEPLAY::BALL::DrawBall();
	GAMEPLAY::BLOCKS::DrawBlocks();
	GAMEPLAY::PAD::DrawLifes();
	GAMEPLAY::PAD::SCORE::DrawScore();

	EndDrawing();
}
void GAME::DeInit()
{		
	UnloadMusicStream(introMusic);
	UnloadMusicStream(gameMusic);
	CloseAudioDevice();
	UnloadTexture(txtr_Pad);
	UnloadTexture(txtr_Ball);
	UnloadTexture(txtr_Block1);
	UnloadTexture(txtr_Block2);
	UnloadTexture(txtr_Block3);
	UnloadTexture(txtr_Block4);
	UnloadTexture(background);
	CloseWindow();
}
void GAME::Play()
{
	Init();

	while (!WindowShouldClose() && !exit)
	{
		switch (currentScene)
		{
		case Scenes::startMenu:
			UpdateMusicStream(introMusic);
			MENU::ShowCursorOnMenu();
			MENU::MainMenuScene();
			MENU::MouseInput(Scenes::game, playRecX_Pos, playRecY_Pos, playRecX_Pos + playRecWidth, playRecY_Pos + playRecHeight, gameMusic, introMusic);
			MENU::MouseInput(Scenes::credits, creditsRecX_Pos, creditsRecY_Pos, creditsRecX_Pos + creditsRecWidth, creditsRecY_Pos + creditsRecHeight);
			MENU::MouseInput(Scenes::exit, exitRecX_Pos, exitRecY_Pos, exitRecX_Pos + exitRecWidth, exitRecY_Pos + exitRecHeight);
			MENU::MouseInput(Scenes::instructions, instructionsRecX_Pos, instructionsRecY_Pos, instructionsRecX_Pos + instructionsRecWidth, instructionsRecY_Pos + instructionsRecHeight);
			break;
		case Scenes::game:			
			Input();
			Update();
			Draw();
			break;	
		case Scenes::win:
			UpdateMusicStream(gameMusic);
			MENU::WinScene();
			MENU::KeyboardInput(KEY_ENTER, Scenes::startMenu, introMusic, gameMusic);
			break;
		case Scenes::loose:
			UpdateMusicStream(gameMusic);
			MENU::LooseScene();
			MENU::KeyboardInput(KEY_ENTER, Scenes::startMenu, introMusic, gameMusic);
			break;
		case Scenes::credits:
			UpdateMusicStream(introMusic);
			MENU::CreditsScene();
			MENU::KeyboardInput(KEY_ENTER, Scenes::startMenu);
			break;
		case Scenes::exit:
			exit = true;
			break;
		case Scenes::pause:
			MENU::ShowCursorOnMenu();
			MENU::PauseMenu();
			MENU::MouseInput(Scenes::game, resumeRecX_Pos, resumeRecY_Pos, resumeRecX_Pos + resumeRecWidth, resumeRecY_Pos + resumeRecHeight);
			MENU::RestartMatchWithMouseInput(Scenes::startMenu, backRecX_Pos, backRecY_Pos, backRecX_Pos + backRecWidth, backRecY_Pos + backRecHeight, introMusic, gameMusic);
			MENU::RestartMatchWithMouseInput(Scenes::game, restartRecX_Pos, restartRecY_Pos, restartRecX_Pos + restartRecWidth, restartRecY_Pos + restartRecHeight);
			break;
		case Scenes::instructions:
			UpdateMusicStream(introMusic);
			MENU::InstructionsScene();
			MENU::KeyboardInput(KEY_ENTER, Scenes::startMenu);
			break;
		}		
	}

	DeInit();	
}