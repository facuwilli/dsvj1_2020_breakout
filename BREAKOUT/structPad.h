#ifndef STRUCTPAD_H

#define STRUCTPAD_H

#include "..\lib\raylib\include\raylib.h"

enum MovementStatus { left, medium, right };
const short scoreLenght = 6;
const short lifeLenght = 1;

struct Pad
{	
	Rectangle rec;
	Color color;
	char score[scoreLenght];	
	float speed;
	MovementStatus moveStatus;
	char lifes[lifeLenght];
};

#endif