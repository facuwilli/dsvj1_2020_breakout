#ifndef GAME_H

#define GAME_H

#include "gameplay.h"
#include "output_vars.h"

namespace ARKANOID
{
	namespace GAME
	{
		enum class Scenes { startMenu, game, win, loose, credits, exit, pause, instructions };
		extern Scenes currentScene;

		extern Music introMusic;
		extern Music gameMusic;

		extern Texture2D txtr_Pad;
		extern Texture2D txtr_Ball;
		extern Texture2D txtr_Block1;
		extern Texture2D txtr_Block2;
		extern Texture2D txtr_Block3;
		extern Texture2D txtr_Block4;
		extern Texture2D background;

		extern bool exit;

		static void Init();
		static void Input();
		static void Update();
		static void Draw();
		static void DeInit();
		void Play();	

		namespace MENU
		{
			void MainMenuScene();
			void WinScene();
			void LooseScene();
			void CreditsScene();
			void PauseMenu();
			void InstructionsScene();
			void KeyboardInput(int key, Scenes scene);
			void KeyboardInput(int key, Scenes scene, Music playMusic, Music stopMusic);
			void MouseInput(Scenes scene, int min_X, int min_Y, int max_X, int max_Y);
			void MouseInput(Scenes scene, int min_X, int min_Y, int max_X, int max_Y, Music playMusic, Music stopMusic);
			void RestartMatchWithMouseInput(Scenes scene, int min_X, int min_Y, int max_X, int max_Y);
			void RestartMatchWithMouseInput(Scenes scene, int min_X, int min_Y, int max_X, int max_Y, Music playMusic, Music stopMusic);
			void ChangeCurrentScene(short scene);
			void ShowCursorOnMenu();
		}
	}
}

#endif