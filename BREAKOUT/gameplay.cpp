#include "gameplay.h"

using namespace ARKANOID;

#pragma region vars
Pad GAMEPLAY::pad;
int GAMEPLAY::moveRight = KEY_RIGHT;
int GAMEPLAY::moveLeft = KEY_LEFT;
Ball GAMEPLAY::ball;
int GAMEPLAY::throwBall = KEY_SPACE;
Block GAMEPLAY::blocks[blocksAmount];
short GAMEPLAY::BALL::blockCollisionIndex = 0;
short GAMEPLAY::BALL::blocksDestroy = 0;
bool GAMEPLAY::BALL::onCollision = false;
#pragma endregion

void GAMEPLAY::RestartObjetsPositions()
{
	pad.rec.x = (float)((screenWidth / 2) - (GAMEPLAY::pad.rec.width / 2));
	ball.status = false;
	ball.position.y = pad.rec.y - 17.0f;	
	ball.speed.x = 200;
	ball.speed.y = -500;
}
void GAMEPLAY::RestartMatch()
{
	PAD::SCORE::InitScore();
	BLOCKS::InitBlocks(18, 18, 18, 18);
	PAD::InitLifes();
	RestartObjetsPositions();
}
void GAMEPLAY::HideCursorIfMatchStarted()
{
	if (!IsCursorHidden())
	{
		HideCursor();
	}
}
void GAMEPLAY::TurnOffMusic()
{
	if(IsKeyPressed(KEY_M))
	{
		if(IsMusicPlaying(GAME::gameMusic))
		{
			PauseMusicStream(GAME::gameMusic);
		}
		else
		{
			ResumeMusicStream(GAME::gameMusic);
		}		
	}
}

void GAMEPLAY::PAD::SCORE::InitScore()
{
	for (short i = 0; i < scoreLenght; i++)
	{
		pad.score[i] = 48;
	}	
}
void GAMEPLAY::PAD::SCORE::DrawScore()
{
	DrawText(scoreText, scoreTextXPos, scoreTextYPos, scoreTextFontSize, scoreTextColor);
	DrawText(pad.score, scoreXPos, scoreYPos, scoreFontSize, scoreColor);
}
void GAMEPLAY::PAD::SCORE::ScoreUp(short index)
{
	short diference = scoreLenght - pointsLenght;

	for (short i = pointsLenght - 1; i >= 0; i--)
	{		
		if (pad.score[i + diference] + blocks[index].destructionPoints[i] > nine)
		{
			pad.score[i + diference - 1] += 1;
			pad.score[i + diference] = zero + ((pad.score[i + diference] + blocks[index].destructionPoints[i]) - ten);

			for (short x = i + diference - 1; x >= 0; x--)
			{
				if(pad.score[x] > nine)
				{
					pad.score[x] = zero;
					pad.score[x-1] += 1;
				}								
			}			
		}
		else
		{
			pad.score[i + diference] += blocks[index].destructionPoints[i];
		}
	}
}

void GAMEPLAY::PAD::InitLifes()
{
	pad.lifes[0] = three;
}
void GAMEPLAY::PAD::InitPad()
{
	pad.rec.height = 20;
	pad.rec.width = 99;
	pad.rec.x = (float)((screenWidth / 2) - (pad.rec.width / 2));
	pad.rec.y = (float)(screenHeight - 80);
	pad.color = WHITE;	
	pad.speed = 300.0f;
	pad.moveStatus = MovementStatus::medium;
	InitLifes();	
}
void GAMEPLAY::PAD::DrawPad()
{	
	DrawTexture(GAME::txtr_Pad, (int)pad.rec.x, (int)pad.rec.y, pad.color);
}
void GAMEPLAY::PAD::DrawLifes()
{
	DrawText(lifesText, lifesTextXPos, lifesTextYPos, lifesTextFontSize, lifesTextColor);
	DrawText(pad.lifes, lifesXPos, lifesYPos, lifesFontSize, lifesColor);
}
bool GAMEPLAY::PAD::LimitsReached(bool side)
{
	switch (side)
	{
	case true:
		return pad.rec.x - leftEdgeWidth - 4 <= leftEdgeXPos;
		break;
	case false:
		return pad.rec.x + pad.rec.width >= rightEdgeXPos;
		break;
	}	

	return true;
}
void GAMEPLAY::PAD::MovementStatus()
{
	if (IsKeyDown(moveRight) && !LimitsReached(false))
	{
		pad.moveStatus = MovementStatus::right;
	}
	else if (IsKeyDown(moveLeft) && !LimitsReached(true))
	{
		pad.moveStatus = MovementStatus::left;
	}
	else
	{
		pad.moveStatus = MovementStatus::medium;
	}
}
void GAMEPLAY::PAD::PadMovement()
{
	switch (pad.moveStatus)
	{
	case MovementStatus::left:
		pad.rec.x -= pad.speed * GetFrameTime();
		break;
	case MovementStatus::medium:
		pad.rec.x += 0;
		break;
	case MovementStatus::right:
		pad.rec.x += pad.speed * GetFrameTime();
		break;
	}	
}
void GAMEPLAY::PAD::AllLifesLost()
{
	if (pad.lifes[0] <= zero)
	{
		GAME::MENU::ChangeCurrentScene(3);
		RestartMatch();		
	}
}

void GAMEPLAY::BALL::InitBall()
{	
	ball.position.x = pad.rec.x + (pad.rec.width / 2); 
	ball.position.x = pad.rec.x + (pad.rec.width / 2 - 15);
	ball.position.y = pad.rec.y - 25.0f;
	ball.color = WHITE;
	ball.radius = 12;
	ball.status = false;
	ball.speed.x = 200;
	ball.speed.y = -500;
}
void GAMEPLAY::BALL::DrawBall()
{	
	DrawTextureV(GAME::txtr_Ball, ball.position, ball.color);	
}
bool GAMEPLAY::BALL::CheckCollisionBlocks()
{
	bool collision = false;

	for(short i=0;i<blocksAmount;i++)
	{
		if (blocks[i].status && CheckCollisionCircleRec(ball.position, ball.radius, blocks[i].rec))
		{
			collision = true;
			blockCollisionIndex = i;
		}
	}

	return collision;
}
void GAMEPLAY::BALL::CoreMechanics()
{
	if(ball.position.y <= 0)
	{
		ball.speed.y *= -1;
	}
	else if (ball.position.y >= 960)
	{
		pad.lifes[0] -= 1;
		RestartObjetsPositions();
		
		PAD::AllLifesLost();
	}

	if (ball.position.x <= 140 || ball.position.x >= 830)
	{
		ball.speed.x *= -1;
	}

	if (CheckCollisionCircleRec(ball.position, ball.radius, pad.rec))
	{
		if (!onCollision)
		{
			ball.speed.y *= -1;
			RandomDirection();
			onCollision = true;
		}		
	}	
	else
	{
		onCollision = false;
	}

	if(CheckCollisionBlocks())
	{
		ball.speed.y *= -1;	
		BALL::RandomDirection();

		BLOCKS::LowerBlockDurability(blockCollisionIndex);

		BLOCKS::DestroyBlock(blockCollisionIndex);

		BLOCKS::WiningAchived();		
	}	
}
void GAMEPLAY::BALL::BallMovement()
{
	switch (ball.status)
	{
	case true:
		CoreMechanics();
		ball.position.y += ball.speed.y * GetFrameTime();
		ball.position.x += ball.speed.x * GetFrameTime();
		break;
	case false:
		ball.position.x = pad.rec.x + (pad.rec.width / 2 - 15);		
		break;
	}
}
void GAMEPLAY::BALL::ThrowBall()
{
	if (IsKeyPressed(throwBall) && !ball.status)
	{
		ball.status = true;
		RandomDirection();
	}
}
void GAMEPLAY::BALL::SpeedUp() 
{
	if (blocksDestroy % 6 == 0)
	{
		ball.speed.x += 40;
		ball.speed.y += 40;
	}
}
void GAMEPLAY::BALL::RandomDirection()
{
	switch(GetRandomValue(1,2))
	{
	case 1:		
		if(ball.speed.x > 0)
		{
			ball.speed.x *= -1;
		}
		break;
	case 2:		
		if(ball.speed.x < 0)
		{
			ball.speed.x *= -1;
		}
		break;
	}	
}

void GAMEPLAY::BLOCKS::InitBlocks(short normals, short resistants, short hards, short veryHards)
{
	float x = 150;
	float y = 150;
	short acumulador = 0;

	Vector2 distanceBetweenEach = {75, 35};	

	for (short i = 0; i < veryHards; i++)
	{
		blocks[i].type = BlockType::reallyHard;
	}

	acumulador += veryHards;

	for (short i = 0; i < hards; i++)
	{
		blocks[i+acumulador].type = BlockType::hard;		
	}

	acumulador += hards;

	for (short i = 0; i < resistants; i++)
	{
		blocks[i+acumulador].type = BlockType::resistant;		
	}

	acumulador += resistants;

	for (short i = 0; i < normals; i++)
	{
		blocks[i+acumulador].type = BlockType::normal;		
	}

	for(short i = 0;i<blocksAmount;i++)	
	{
		blocks[i].rec.height = 30;
		blocks[i].rec.width = 70;
		blocks[i].rec.x = x;		
		blocks[i].rec.y = y;
		blocks[i].status = true;
		blocks[i].color = WHITE;

		switch (blocks[i].type)
		{
		case BlockType::normal:
			blocks[i].durability = 1;
			blocks[i].destructionPoints[0] = 0;
			blocks[i].destructionPoints[1] = 2;
			blocks[i].destructionPoints[2] = 5;						
			break;
		case BlockType::resistant:
			blocks[i].durability = 2;
			blocks[i].destructionPoints[0] = 0;
			blocks[i].destructionPoints[1] = 5;
			blocks[i].destructionPoints[2] = 0;					
			break;
		case BlockType::hard:
			blocks[i].durability = 3;
			blocks[i].destructionPoints[0] = 0;
			blocks[i].destructionPoints[1] = 7;
			blocks[i].destructionPoints[2] = 5;						
			break;
		case BlockType::reallyHard:
			blocks[i].durability = 4;
			blocks[i].destructionPoints[0] = 1;
			blocks[i].destructionPoints[1] = 0;
			blocks[i].destructionPoints[2] = 0;					
			break;		
		}	
		
		if (x + distanceBetweenEach.x >= 800)
		{
			y += distanceBetweenEach.y;
			x = 150;
		}
		else
		{
			x += distanceBetweenEach.x;
		}
	}
}
void GAMEPLAY::BLOCKS::DrawBlocks()
{
	for(short i = 0;i<blocksAmount;i++)
	{
		if(blocks[i].status)
		{
			switch (blocks[i].type)
			{
			case BlockType::normal:
				DrawTexture(GAME::txtr_Block1, (int)blocks[i].rec.x, (int)blocks[i].rec.y, blocks[i].color);
				break;	
			case BlockType::resistant:
				DrawTexture(GAME::txtr_Block2, (int)blocks[i].rec.x, (int)blocks[i].rec.y, blocks[i].color);
				break;
			case BlockType::hard:
				DrawTexture(GAME::txtr_Block3, (int)blocks[i].rec.x, (int)blocks[i].rec.y, blocks[i].color);
				break;
			case BlockType::reallyHard:
				DrawTexture(GAME::txtr_Block4, (int)blocks[i].rec.x, (int)blocks[i].rec.y, blocks[i].color);
				break;
			}			
		}		
	}
}
void GAMEPLAY::BLOCKS::DestroyBlock(short index)
{
	if (blocks[index].durability <= 0)
	{
		blocks[index].status = false;
		PAD::SCORE::ScoreUp(index);
		BALL::blocksDestroy += 1;
		BALL::SpeedUp();
	}	
}
bool GAMEPLAY::BLOCKS::AllBlocksDestroy()
{
	bool allDestroy = true;

	for (short i = 0; i < blocksAmount; i++)
	{		
		if (blocks[i].status == true)
		{
			allDestroy = false;
			i = blocksAmount;
		}
	}

	return allDestroy;
}
void GAMEPLAY::BLOCKS::LowerBlockDurability(short blockIndex)
{
	blocks[blockIndex].durability -= 1;

	switch (blocks[blockIndex].type)
	{
	case BlockType::reallyHard:
		blocks[blockIndex].type = BlockType::hard;		
		break;
	case BlockType::hard:		
		blocks[blockIndex].type = BlockType::resistant;
		break;
	case BlockType::resistant:		
		blocks[blockIndex].type = BlockType::normal;
		break;
	}
}
void GAMEPLAY::BLOCKS::WiningAchived()
{
	if (BLOCKS::AllBlocksDestroy())
	{
		GAME::MENU::ChangeCurrentScene(2);
		RestartMatch();		
	}
}