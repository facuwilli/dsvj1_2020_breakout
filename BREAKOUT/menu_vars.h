#pragma once
#include "raylib.h"

const int screenWidth = 960;
const int screenHeight = 960;

#pragma region Main Menu Vars
const short playRecWidth = 275;
const short playRecHeight = 100;
const float playRecX_Pos = screenWidth / 2 - (playRecWidth / 2);
const float playRecY_Pos = (screenHeight / 2) - 300;
const Color playRecColor = RED;

const short creditsRecWidth = 275;
const short creditsRecHeight = 100;
const float creditsRecX_Pos = screenWidth / 2 - (playRecWidth / 2);
const float creditsRecY_Pos = (screenHeight / 2) - 150;
const Color creditsRecColor = RED;

const char playText[6] = { "START" };
const float playTextFontSize = 50;
const float playTextX_Pos = playRecX_Pos + (playRecWidth / 6);
const float playTextY_Pos = playRecY_Pos + (playRecHeight / 4);
const Color playTextColor = DARKBLUE;

const char creditsText[8] = { "CREDITS" };
const float creditsTextFontSize = 50;
const float creditsTextX_Pos = playRecX_Pos + (playRecWidth / 6) - 20;
const float creditsTextY_Pos = playRecY_Pos + (playRecHeight / 4) + 150;
const Color creditsTextColor = DARKBLUE;
#pragma endregion